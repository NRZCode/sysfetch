#!/usr/bin/env bash
get_width() {
  w=0
  for t; do
    w=$((w > ${#t} ? w : ${#t}))
  done
  ((w+=2))
}

get_login() {
  echo "$USER@$HOSTNAME"
}

get_os() {
  for file in /etc/lsb-release /usr/lib/os-release /etc/os-release /etc/openwrt_release; do
    source "$file" && break
  done
  echo "$DISTRIB_DESCRIPTION"
}

get_host() {
  printf '%s %s MB:%s\n' "$(</sys/devices/virtual/dmi/id/board_vendor)" \
    "$(</sys/devices/virtual/dmi/id/product_version)" \
    "$(</sys/devices/virtual/dmi/id/board_name)"
}

get_kernel() {
  uname -srm
}

get_uptime() {
  : "$(uptime -p)"
  echo "${_#* }"
}

get_packages() {
  echo "$(dpkg -l|wc -l) (dpkg)"
}

get_shell() {
  shell=$(ps -ho command -p $PPID)
  case $shell in
    bash) shell+=" $BASH_VERSION";;
  esac
  echo "$shell"
}

get_resolution() {
  xrandr|sed -n -E '/current/s/.*current ([0-9]+) x ([0-9]+).*/\1x\2/p'
}

declare -A result=(
  [Login]=$(get_login)
  [OS]=$(get_os)
  [Host]=$(get_host)
  [Kernel]=$(get_kernel)
  [Uptime]=$(get_uptime)
  [Packages]=$(get_packages)
  [Shell]=$(get_shell)
  [Resolution]=$(get_resolution)
)

get_width "${!result[@]}"
for k in Login OS Host Kernel Uptime Packages Shell Resolution; do
  printf "%${w}s: %s\n" "$k" "${result[$k]}"
done
